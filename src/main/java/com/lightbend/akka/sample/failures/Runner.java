package com.lightbend.akka.sample.failures;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;

public class Runner {
    public static void main(String[] args) {
        ActorSystem system = ActorSystem.create("lifecycleSystem");

        ActorRef supervisingActor = system.actorOf(SupervisingActor.props(), "supervising-actor");
        supervisingActor.tell("failChild", ActorRef.noSender());
    }
}

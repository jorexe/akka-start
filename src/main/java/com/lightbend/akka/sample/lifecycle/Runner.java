package com.lightbend.akka.sample.lifecycle;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.PoisonPill;

public class Runner {
    public static void main(String[] args) {
        ActorSystem system = ActorSystem.create("lifecycleSystem");

        ActorRef first = system.actorOf(StartStopActor1.props(), "first");
        first.tell(PoisonPill.getInstance(), ActorRef.noSender());
    }
}
